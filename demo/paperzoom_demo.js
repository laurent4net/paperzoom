

window.globals = {};

// example shapes (for demo)
var line = new Path();
line.add(new Point(200,200));
line.strokeColor = 'red';
line.strokeWidth = 3;
line.add(new Point(350,350));

var circle = new Path.Circle(new Point(100, 200), 100);
circle.strokeColor = 'black';

var circle_at_center_of_view = new Path.Circle(new Point(view.center), 10 );
circle_at_center_of_view.fillColor = 'green';


// Pan
// tool.minDistance = 2; // (default = 1)
function onMouseDrag(event) {
    project.view.scrollBy(event.downPoint - event.point);
}


// track mouse position
function onMouseMove(event) {
    mousePos = event.point;
}

// zoom using mousewheel event.deltaY
// received from jquery-mousewheel
globals.zoom = function (event) {
     // Uncomment both lines below to zoom only if a defined key is down
     // (z key in this example). Use 'option' for Alt key
     // avoid 'Ctrl' as it is usually also the browser zoom key
     // 'shift' gave me some problem when I tested because
     // deltaX and deltaY are exchanged at least in firefox to produce
     // horizontal scroll with shift-wheel.

     // if (Key.isDown('z')) {
        paperzoom(event, project, mousePos);
     // };
}

function onKeyDown(event) {
    if (event.key == 'space') {
        // zoom reset
        paperzoom(0, project, mousePos);
        // Prevent the key event from bubbling
        //return false;
    }
}
