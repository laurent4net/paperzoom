/*!
 * paperzoom.js - fixed position zooming on paperjs canvas
 * https://bitbucket.org/laurent4net/paperzoom
 *
 *
 * Copyright (c) 2014 Laurent Rondet
 * Released under the MIT license
 * (Please see LICENSE.txt for details)
 *
 * Date: 2014-05-05
 */

/*
 * Please see README.md for instructions
 *
 */

// zoom
paperzoom = function (zoomType, caller, fixedPoint, zoomFactor) {

    // 10% zoom factor on each step if not supplied
    if (typeof zoomFactor === 'undefined') { zoomFactor = 1.1; }

    if (zoomFactor == 0) { zoomFactor = 1; } // does nothing

    // negative zoomType -> invert zoomFactor (zoom-out)
    if ( zoomType < 0 ) { zoomFactor = 1 / zoomFactor; }
    // if 0 -> reset zoom
    else if ( zoomType == 0) { zoomFactor = 1 / caller.view.zoom; }

    // apply zoom to the caller view
    caller.view.zoom *= zoomFactor;

    // vector from fixed point to translate to view center where zoom is stable
    var vector = new Point(new Point(fixedPoint) - new Point(caller.view.center));

    // invert zoom effect on vector ( = divide by factor)
    // and add to original vector in opposite direction ( = substract)
    // vector = vector - new Point(vector / zoomFactor);
    // simplification:
    vector = vector * (1 - 1 / zoomFactor);
    // translate fixed point back to original screen position (under mouse cursor)
    caller.view.scrollBy(vector);
}
