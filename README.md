**paperzoom.js - fixed position zooming on paperjs canvas**
--------------------

https://bitbucket.org/laurent4net/paperzoom

Copyright (c) 2014 Laurent Rondet

Released under the MIT license (Please see LICENSE.txt for details)


Date: 2014-05-05
 
 

**Calling**:


```
#!javascript

paperzoom(event, project, fixedPoint);
```


 

**Comments**:

*zoomType*:   numeric value. 

* positive for zoom-in

* negative for zoom-out

* 0 = no zoom (keeps fixedPoint at same position)

* value itself is not the zoom factor (so 1 and 100 will have the same effect)


*caller*:     where to apply the zoom (usually: project for current project)

*fixedPoint*: the point you want to keep fixed on the screen (mouse pointer position is a good choice for example)

*zoomFactor*: optional, default 1.1 for 10% zoom step

* zoomFactor > 1: increase zoom

* 0 < zoomFactor < 1: reduce zoom

* 0 and 1 = nothing

* negative values will zoom properly but will also apply a center point symetry (on fixedPoint) on each step (so odd steps will be symetric and even steps will return to normal)


Use:
-----
Bind mouse wheel event to a function that calls the paperzoom function. (See demo for more details)
